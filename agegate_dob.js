/**
 * @file
 * A JavaScript file for agegate_dob module.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

$(document).ready(function() {

  injectAgegate();

  validateAgegate();

});//End of document ready

function injectAgegate() {

  var agegate_dob_years = '<input name="agegate_dob_years" id="agegate_dob_years" placeholder="YYYY" type="text" maxlength="4" />';
  var agegate_dob_months = '<input name="agegate_dob_months" id="agegate_dob_months" placeholder="MM" type="text" maxlength="2" />';
  var agegate_dob_days = '<input name="agegate_dob_days" id="agegate_dob_days" placeholder="DD" type="text" maxlength="2" />';

  var agegate_dob_content = '<form id="agegate_dob_form">';
  agegate_dob_content += '<div id="agegate_dob_deny_message"></div>';
  agegate_dob_content += '<div class="agegate_dob_before">You need to be 18 or older to enter.</div>';
  agegate_dob_content += '<div class="agegate_dob_dob">';
  agegate_dob_content += agegate_dob_days;
  agegate_dob_content += agegate_dob_months;
  agegate_dob_content += agegate_dob_years;
  agegate_dob_content += '</div>';
  agegate_dob_content += '<div class="agegate_dob_after">Please enter your date of birth for age verification.</div>';
  agegate_dob_content += '<input type="submit" id="agegate_dob_submit" value="Enter Here" name="agegate_dob_submit">';
  agegate_dob_content += '</form>';

  $('#agegate_dob').html(agegate_dob_content);

  // Set some style.
  $('#agegate_dob').attr('style', 'position: fixed; top: 0; left: 0; background: #fff; z-index: 9999;');

  // Set width and height based on screen proportions.
  $('#agegate_dob').css('height', screen.height);
  $('#agegate_dob').css('width', screen.width);

  //$('#agegate_dob *').uniform();

  $('#agegate_dob_years, #agegate_dob_months, #agegate_dob_days, #agegate_dob_submit').autotab_magic().autotab_filter('numeric');

}

function validateAgegate() {

  $('#agegate_dob_form').submit(function( event ) {

    // Perform basic validation
    var validate = 'allow';
    var validation_message = '';
    if($('#agegate_dob_years').val() == '') { validation_message += 'No year entered.<br>'; validate = 'deny'; }
    if($('#agegate_dob_months').val() == '') { validation_message += 'No month entered.<br>'; validate = 'deny'; }
    if($('#agegate_dob_days').val() == '') { validation_message += 'No day entered.<br>'; validate = 'deny'; }

    if(validate == 'deny') {
      $('#agegate_dob_deny_message').html(validation_message);
      return false;
    }

    var allow_deny = 'deny';
    var deny_message = '';

    // Deny people who enter silly year dates.
    var current_year = new Date().getFullYear() - 1;
    var entered_year = $('#agegate_dob_years').val()
    if(entered_year > current_year) { deny_message += 'You can\'t be that young!<br>'; }

    // Check date of birth makes person older than 18.
    var current_timestamp = $('#agegate_dob').attr('data-timestamp') * 1000 - 61200000; // Convert to milliseconds and remove 17 hours.
    console.log(current_timestamp);

    // Get a timestamp in milliseconds from the entered dates.
    var dob = $('#agegate_dob_days').val() + '-' + $('#agegate_dob_months').val() + '-' + $('#agegate_dob_years').val();
    dob = dob.split("-");
    dob = dob[1] + "," + dob[0] + "," + dob[2];
    dob = new Date(dob).getTime()
    dob = dob;

    var second_difference = (current_timestamp - dob) / 1000;

    // Check if the difference is greater than 18 years.
    if(second_difference > 568024668) {
      allow_deny = 'allow';
    } else {
      deny_message += 'Sorry, you\'re not 18';
    }

    // Check for deny, alert user or accept and set cookie.
    if(allow_deny == 'deny') {
      $('#agegate_dob_deny_message').html(deny_message);
    } else {
      $('#agegate_dob').remove();
      $.cookie('agegate_dob_accepted', 1, { expires : 10, path: '/' });
    }

    event.preventDefault();
    return false;

  });

}

})(jQuery, Drupal, this, this.document);
